# Description

# How To Use This Skeleton

1. Clone this repository locally (skip if the repository is already cloned)
2. `rsync -av ./ <destination directory>`
3. In the destination directory:
    1. Copy the _src/*.cljs_ files to the proper location and update their
       namespaces
    2. Update the `:init-fn` in _shadow-cljs.edn_
    3. Change "name" in _package.json_
    4. Change the `title` in _resources/index.html_
    5. `npm i`
    6. Remove this section from the README

# Development

Run `npm run dev` and open http://localhost:8280 in a browser.
