(ns events
  (:require
    [re-frame.core :refer [reg-event-fx reg-event-db reg-fx]]
    [utils :as utils]))

(reg-fx
  :set-route
  (fn [{:keys [url]}]
    (utils/set-hash! url)))

(reg-event-fx
  ::initialize-app
  (fn [_ _]
    {:db {}}))

(reg-event-fx
  ::route-dispatch
  (fn [{:keys [db]} [_ route-info]]
    (let [route-id (:handler route-info)]
      (case route-id
        :home {:dispatch [::set-active-panel :home-panel]}
        :game-board {:dispatch [::set-active-panel :game-board]}
        {:dispatch [::set-active-panel :invalid-route]}))))

(reg-event-db
  ::set-active-panel
  (fn [db [_ active-panel]]
    (assoc db :active-panel active-panel)))