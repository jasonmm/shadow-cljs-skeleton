(ns views
  (:require
    [re-frame.core :as re]
    [goog.string :as gstring]
    [routes :as routes]
    [subs :as subs]
    [events :as events]
    [utils :refer [<sub]]))

(defn- container
  [body]
  [:div.flex.flex-col.justify-center.items-center.max-w-md.m-6.pb-10.mx-auto body])

(defn home-panel []
  [container
   [:<>
    [:span "New App"]]])

(defn main-panel
  []
  (case (<sub [::subs/active-panel])
    :home-panel [home-panel]
    [:div "Invalid panel"]))
